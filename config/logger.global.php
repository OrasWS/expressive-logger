<?php

use Psr\Log\LoggerInterface;

use Monolog\Logger;
use Monolog\Handler as MonologHandler;
use Worldstores\ExpressiveLogger\Handler;
use Worldstores\ExpressiveLogger\Processor;

return [
    'dependencies' => [
        'invokables' => [
            'GetStreamHandler' => Handler\GetStreamHandler::class
        ],
    ],
    
    'logger' => [
        LoggerInterface::class => [
            'handlers' => [
                [
                    'name' => MonologHandler\StreamHandler::class,
                    'path' => './data/log/application.log',
                    'level' => Logger::DEBUG,
                    'bubble' => true,
                ]
            ],
            'processors' => [Processor\Psr7RequestProcessor::class],
            'formatters' => []
        ],
    ]
];
