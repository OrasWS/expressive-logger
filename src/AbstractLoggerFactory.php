<?php
namespace Worldstores\ExpressiveLogger;

use Interop\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

use Monolog\Logger;


/**
 * Class AbstractLoggerFactory
 * @package V1\Infrastructure\Logger
 */
class AbstractLoggerFactory implements AbstractFactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return Logger
     * @throws ServiceNotFoundException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        if (empty($config['logger'][$requestedName])) {
            throw new \LogicException(sprintf('Missing config for %s', $requestedName));
        }

        $logger = new Logger($requestedName);
        $loggerConfig = $config['logger'][$requestedName];

        if (!empty($loggerConfig['handlers'])) {
            foreach ($loggerConfig['handlers'] as $args) {
                if (!$container->has($args['name'])) {
                    throw new ServiceNotFoundException(sprintf(
                        "Service %s not found",
                        $args['name']
                    ));
                }

                $handler = ($container->get($this->getHandlerClassName($args['name'])))($args); 
                $logger->pushHandler($handler);
            }
        }

        if (!empty($loggerConfig['processors'])) {
            foreach ($loggerConfig['processors'] as $processor) {
                $logger->pushProcessor($container->get($processor));
            }
        }

        if ($requestedName == LoggerInterface::class) {
            return $logger;
        }

        return new $requestedName($logger);
    }

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @return mixed
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        $classExists = class_exists($requestedName);

        $implementsLoggerInterface = false;
        if ($classExists) {
            $implementsLoggerInterface = in_array(LoggerInterface::class, class_implements($requestedName));
        }

        $isLoggerInterface = $requestedName == LoggerInterface::class;

        return $isLoggerInterface || $implementsLoggerInterface;
    }


    /**
     * @param $handlerNamespace
     * @return mixed
     */
    private function getHandlerClassName($handlerNamespace)
    {
        $namespaces = explode("\\", $handlerNamespace);
        return sprintf('Get%s', end($namespaces));
    }
}