<?php
namespace Worldstores\ExpressiveLogger\Handler;

use Monolog\Handler\StreamHandler;

/**
 * Class GetStreamHandler
 * @package V1\Infrastructure\Logger\Handler
 */
class GetStreamHandler
{
    public function __invoke($handlerConfig) : StreamHandler
    {
        return new StreamHandler($handlerConfig['path'], $handlerConfig['level'], $handlerConfig['bubble']);
    }
}
