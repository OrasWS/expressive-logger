<?php
namespace Worldstores\ExpressiveLogger\Processor;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Psr7RequestProcessor
 * @package V1\Infrastructure\Middleware\Api\Logger
 */
class Psr7RequestProcessor
{
    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return array
     */
    private function createContext(RequestInterface $request, ResponseInterface $response)
    {
        return [
            'request' => [
                'method' => $request->getMethod(),
                'uri' => $request->getUri(),
                'query' => $request->getParsedBody(),
                'body' => $request->getBody()->getContents()
            ],
            'response' => [
                'statusCode' => $response->getStatusCode(),
                'reasonPhrase' => $response->getReasonPhrase(),
                'body' => $response->getBody()->getContents()
            ]
        ];
    }

    /**
     * @param array $record
     * @return array
     */
    public function __invoke(array $record)
    {
        $context = $record['context'];
        if (!empty($context['request']) && $context['request'] instanceof RequestInterface &&
            !empty($context['response']) && $context['response'] instanceof ResponseInterface
        ) {
            $record['context'] = $this->createContext($context['request'], $context['response']);
        }

        return $record;
    }
}